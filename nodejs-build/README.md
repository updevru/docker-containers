### Usage

    docker run -it --rm updev/nodejs-build

#### Run `node`

    docker run -it --rm updev/nodejs-build node

#### Run `npm`

    docker run -it --rm updev/nodejs-build npm

#### Run `bower`

    docker run -it --rm updev/nodejs-build bower

#### Run `grunt`

    docker run -it --rm updev/nodejs-build grunt