### Create certificate

    docker run --rm -it -p 80:80 -p 443:443 -v /etc/letsencrypt:/etc/letsencrypt updev/letsencrypt certonly --standalone --email test@example.com -d example.com

### Renew certificate need stopping web server

    docker run --rm -it -p 80:80 -p 443:443 -v /etc/letsencrypt:/etc/letsencrypt updev/letsencrypt renew

### Renew certificate without stopping web server

    docker run --rm -v /etc/letsencrypt:/etc/letsencrypt -v /var/www/example.com:/var/www updev/letsencrypt certonly --webroot -w /var/www -d example.com
    
After update certificate need reload web server. More info on https://letsencrypt.org/getting-started/